Counter.js is a simple counter in javascript
============================================

Counter.js is a simple counter in javascript.

By default, the frequency is set to minutes. It can be change using `setResolution()` with the parameter 's' for seconds or 'm' for minutes.

It is possible to define a callback function for each "tick" using `setCallback()`with a function name as parameter.
The function must take only one parameter which is a number.

We start and stop the counter using `start()` and `stop()`.

Example code
------------

    var log = function( counter ){ console.log( counter ) }
    
    window.onload = function(){
        var counter = new c('m');
        counter.setFrequency('s').start().setCallback( log );
    }

Todos
-----

- Add a function which return a human readable counter (hh:mm:ss) according to timer resolution.
- Callback function receive the Timer object, not the number of tick (counter) since start.

Ideas
------