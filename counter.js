function c( frequency ) {

    frequency = typeof frequency !== 'undefined' ? frequency : 'm';

    if (window === this) {
        return new c( frequency );
    }

    this.setFrequency(frequency);
    this._callback = null;
    this._cnt = 0;

    return this;
}


/**
 * Counter
 *  - start : start/continue counter (chainable)
 *  - stop : stop timer (chainable)
 *  - setFrenquency : set frequency for counter (seconds or minutes)
 *  - getCounter : return the current 'count of tick'
 *  - setCallback : set a callback function for each tick
 *  - toString : Override toString in order to return current count
 */
c.prototype = {
    setCallback: function (callback) {
        if(typeof callback === 'function') { 
            this._callback = callback;
        }
        return this;
    },
    setFrequency: function (frequency) {

        if (typeof frequency === 'string' ) {
            if( frequency == 'm') {
                this._frequency = 'm'
                this._interval = 60000; //60,000 milliseconds = 1 minute
            } else {
                this._frequency = 's'
                this._interval = 1000;  //1,000 milliseconds = 1 second                
            }    
        }
        return this;
    },
    start: function () {
        var self = this;
        this._check = setInterval( function () {
            self._cnt += 1;
            if (self._callback) {
                self._callback (self._cnt);
            }
        }, self._interval); 
        return this;
    },    
    stop: function() {
        clearInterval(this._check);
        return this;
    },
    getCounter: function() {
        return this._cnt;
    },
    toString: function() {
        return this.getCounter();
    }
}